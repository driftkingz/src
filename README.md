The file structure is as follows:

Master: One-button functionality (contains main.c.old which is statemachine version of main.c)
- Statemachine: Multi-button functionality
- Lightsensor: Added one input pin and one output pin, plan to add code for light sensor

main.c is located in root\src

INPUT:
IN_UP:    PA4 - A3
IN-LEFT:  PA3 - A2
IN_DOWN:  PA1 - A1
IN_RIGHT: PA0 - A0
IN_LIGHT: PA5 - A4

OUTPUT:
OUT_UP:   PA8 - D9
OUT_LEFT: PA11- D10
OUT_DOWN: PB5 - D11
OUT_RIGHT:PB4 - D12
OUT_LIGHT:PF1 - D8